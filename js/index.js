$(function () {
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    $(".carousel").carousel({
        interval: 2000
    });

    $("#contacto").on("show.bs.modal", function (e) {

        console.log("el modal se esta mostrando")
        $("#ContactoBtn").removeClass("btn-outline-success");
        $("#ContactoBtn").addClass("btn-primary");
        $("#ContactoBtn").prop("disabled", true);
    });
    $("#contacto").on("shown.bs.modal", function (e) {

        console.log("el modal se mostro")

    });
    $("#contacto").on("hide.bs.modal", function (e) {

        console.log("el modal se esta oculto")

    });
    $("#contacto").on("hidden.bs.modal", function (e) {
        $("#ContactoBtn").removeClass("btn-primary");
        $("#ContactoBtn").addClass("btn-outline-success");

        $("#ContactoBtn").prop("disabled", false);
        console.log("el modal se oculto")

    });
});